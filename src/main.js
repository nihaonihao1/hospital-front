import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
// 引入axios
import axios from 'axios'

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
Vue.use(ElementUI);


// 告诉axios,每次请求都携带cookie!
axios.defaults.withCredentials = true;
// 设置axios请求前缀
axios.defaults.baseURL = "http://localhost:8080/";

// 引入ztree
import $ from 'jquery'
import 'ztree'
import 'ztree/css/zTreeStyle/zTreeStyle.css'

Vue.config.productionTip = false
// 把axios挂在到Vue原型上，此后所有的Vue组件
// 都可以通过$axios属性来访问这里引入的axios
// 注意，以下的$axios是随便起的名字，你也可以定为$abc，或者$xyz等等
Vue.prototype.$axios = axios;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
